"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = {
    EnableReduxLogger: process.env.NODE_ENV !== "production"
};
