/// <reference types="react" />
import * as React from "react";
import "./Channel.less";
import * as ChannelState from "../../state/ChannelState";
import * as SessionState from "../../state/SessionState";
export interface Props {
    readonly channel: ChannelState.ChannelState;
    readonly session: SessionState.SessionState;
}
export declare class Channel extends React.PureComponent<Props, undefined> {
    static readonly displayName: string;
    private _messageList;
    private _mainAreaStyleConfig;
    render(): JSX.Element;
    componentDidMount(): void;
    componentWillUnmount(): void;
    private setMessageListRef;
    private renderChrome();
    private renderHeader();
    private handleMessageListLoadMoreMessages;
    private handleConsumeMessageAtIndex;
    private handleInputRowCountChanged;
    private handleInputMessageSent;
    private handleInputFocus;
    private handleWindowResize;
}
