"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var pacomo_1 = require("../../utils/pacomo");
var StyleTag_1 = require("../../utils/StyleTag");
var Globalization_1 = require("../../Globalization");
var AppConfig = require("../../state/ChatConfig");
var Icons_1 = require("../icons/Icons");
var ChannelState = require("../../state/ChannelState");
require("./Input.less");
var Input = Input_1 = (function (_super) {
    __extends(Input, _super);
    function Input(props) {
        var _this = _super.call(this, props) || this;
        _this.textAreaName = "TextArea";
        _this._inputAreaStyleConfig = AppConfig.tryGet(function () { return AppConfig.current().channel.visual.override.inputArea; });
        _this._sendButtonAreaStyleConfig = AppConfig.tryGet(function () { return AppConfig.current().channel.visual.override.sendButton; });
        _this.setTextAreaRef = function (element) {
            _this._inputElement = element;
        };
        _this.handleInputSubmit = function (e) {
            _this.sendMessage();
            e.preventDefault();
        };
        _this.handleInputKeyPress = function (e) {
            if (e.which === 13 && !e.shiftKey && AppConfig.current().channel.returnKeySendsMessage) {
                _this.sendMessage();
                e.preventDefault();
            }
        };
        _this.handleInputFocus = function (e) {
            if (_this.props.onFocus) {
                _this.props.onFocus(e);
            }
        };
        _this.handleInputInput = function (e) {
            var previousInputText = _this.props.channel.inputText;
            if (previousInputText === _this._inputElement.value)
                return;
            ChannelState.Actions.sendTyping(_this.props.channel, _this._inputElement.value);
            _this.adjustRowCount();
        };
        _this.adjustRowCount = function () {
            if (_this._inputElement == undefined) {
                return;
            }
            var currentRows = _this._inputElement.rows;
            _this._inputElement.rows = 1;
            var ratio = Math.round(_this._inputElement.scrollHeight / _this.baseInputScrollHeight);
            var rows = Math.ceil(ratio);
            rows = Math.min(rows, 6);
            _this._inputElement.rows = rows;
            // invalidate the scroll position of message list when input area size changed
            if (currentRows != rows) {
                _this.props.onRowCountChanged();
            }
        };
        var placeholderColor = AppConfig.tryGet(function () { return AppConfig.current().channel.visual.override.inputArea.placeholder.color; });
        if (placeholderColor) {
            _this._inputAreaPlaceholderStyleConfig = _this.createPlaceholderOverrideStyle(placeholderColor);
        }
        return _this;
    }
    Input.prototype.render = function () {
        var sendButtonDisabled = this.props.session.connectionState !== "connected" || !this.props.channel.inputText.trim() ||
            !this.props.channel.isMessageSendingEnabled;
        var disabledReason = !this.props.channel.isMessageSendingEnabled ? (this.props.channel.messageSendingDisabledReason || Globalization_1.strings().MessageSendingDisabled) : undefined;
        var placeholderText = disabledReason || Globalization_1.strings().InputPlaceHolder;
        var sendButtonContainerClass = "SendButtonContainer" + (sendButtonDisabled ? " SendButtonContainer-Disabled" : "");
        return (React.createElement("div", { className: AppConfig.current().channel.visual.inputAreaStyle },
            React.createElement("div", { className: "UpperLine" }),
            React.createElement("div", { className: "LowerArea" },
                React.createElement("div", { className: "TextAreaContainer" },
                    this._inputAreaPlaceholderStyleConfig,
                    React.createElement("textarea", { className: this.textAreaName, ref: this.setTextAreaRef, rows: 1, placeholder: placeholderText, onKeyPress: this.handleInputKeyPress, onInput: this.handleInputInput, onFocus: this.handleInputFocus, value: this.props.channel.inputText, style: this._inputAreaStyleConfig, disabled: !this.props.channel.isMessageSendingEnabled })),
                React.createElement("div", { className: sendButtonContainerClass },
                    React.createElement("button", { type: "button", className: "SendButton", style: this._sendButtonAreaStyleConfig, onClick: this.handleInputSubmit, disabled: sendButtonDisabled, title: disabledReason },
                        React.createElement(Icons_1.SendIcon, { style: this._sendButtonAreaStyleConfig })),
                    React.createElement("div", { className: "ButtonOverlay" })))));
    };
    Input.prototype.sendMessage = function () {
        if (!this.props.channel)
            return;
        if (!this.props.channel.isMessageSendingEnabled) {
            console.warn("Message sending is not enabled");
            return;
        }
        var body = this.props.channel.inputText.trim();
        if (body == "")
            return;
        ChannelState.Actions.sendMessage(this.props.channel);
        this.props.onMessageSent();
    };
    Input.prototype.componentDidMount = function () {
        this.baseInputScrollHeight = this._inputElement.scrollHeight;
    };
    Input.prototype.componentDidUpdate = function () {
        this.adjustRowCount();
    };
    Input.prototype.createPlaceholderOverrideStyle = function (color) {
        var textAreaName = "." + pacomo_1.packageName + "-" + Input_1.displayName + "-" + this.textAreaName;
        var inputPlaceholderTags = ["::-webkit-input-placeholder", ":-moz-placeholder", "::-moz-placeholder", ":-ms-input-placeholder"].map(function (suffix) {
            return ("\n    ." + pacomo_1.packageName + " " + textAreaName + suffix + " {\n        color: " + color + ";\n    }");
        }).join("");
        return React.createElement(StyleTag_1.StyleTag, { content: inputPlaceholderTags });
    };
    return Input;
}(React.PureComponent));
Input.displayName = "ChannelInput";
Input = Input_1 = __decorate([
    pacomo_1.pacomoDecorator
], Input);
exports.default = Input;
var Input_1;
