/// <reference types="react" />
import * as React from "react";
import * as ChannelState from "../../state/ChannelState";
import * as SessionState from "../../state/SessionState";
import "./Input.less";
export interface Props {
    readonly channel: ChannelState.ChannelState;
    readonly session: SessionState.SessionState;
    readonly onRowCountChanged: Function;
    readonly onMessageSent: Function;
    readonly onFocus: Function;
}
export default class Input extends React.PureComponent<Props, undefined> {
    static readonly displayName: string;
    private readonly textAreaName;
    private _inputElement;
    private baseInputScrollHeight;
    private _inputAreaStyleConfig;
    private _inputAreaPlaceholderStyleConfig;
    private _sendButtonAreaStyleConfig;
    constructor(props: Props);
    render(): JSX.Element;
    private setTextAreaRef;
    private sendMessage();
    componentDidMount(): void;
    componentDidUpdate(): void;
    private handleInputSubmit;
    private handleInputKeyPress;
    handleInputFocus: (e: any) => void;
    handleInputInput: (e: React.FormEvent<HTMLTextAreaElement>) => void;
    private adjustRowCount;
    private createPlaceholderOverrideStyle(color);
}
