/// <reference types="react" />
import * as React from "react";
import "./Header.less";
export interface Props {
    readonly channelName: string;
}
export default class Header extends React.PureComponent<Props, undefined> {
    static readonly displayName: string;
    private _styleConfig;
    render(): JSX.Element;
}
