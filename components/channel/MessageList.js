"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var pacomo_1 = require("../../utils/pacomo");
var utils_1 = require("../../utils/utils");
var AppConfig = require("../../state/ChatConfig");
var Globalization_1 = require("../../Globalization");
require("./MessageList.less");
var ChannelItem_1 = require("./ChannelItem");
var Utils = require("../../utils/utils");
var MessageList = (function (_super) {
    __extends(MessageList, _super);
    function MessageList(props) {
        var _this = _super.call(this, props) || this;
        // whether we have messages is props that need to be rendered
        _this.loadedMessagesInUpdate = false;
        // scroll handling related
        _this.keepScrolledToBottom = true;
        _this.keepTopScrollPosition = false;
        _this.lastScrollHeight = 0;
        _this.lastScrollTop = 0;
        // scroll and message list jump handing for touch devices related
        _this.isTouchActive = false;
        _this.isScrollActive = false;
        _this.needsForceRenderAfterScroll = false;
        _this.onAfterScrollTimer = function () {
            _this.isScrollActive = false;
            // if there was a pending update during scroll, then force update now when scroll has fixed position
            if (_this.needsForceRenderAfterScroll && !_this.isTouchActive) {
                _this.forceUpdate();
            }
        };
        _this.onScroll = function () {
            var scrollTop = _this.messageList.scrollTop;
            // do not load messages if we have some pending already
            // if we are in embedded mode, then load messages only when we get to top.
            // in web mode, load messages if there is less than client height of messages to scroll up left
            var loadMore = !_this.loadedMessagesInUpdate &&
                (AppConfig.current().embedded ? scrollTop == 0 : scrollTop <= _this.messageList.clientHeight);
            if (loadMore) {
                _this.loadMoreMessages();
            }
            // store whether we were scrolled to bottom
            _this.captureBottomScrollState();
            // consume last message when scrolled to bottom
            _this.consumeLastMessageIfApplicable();
            // schedule delayed timer to force render if updated were postponed while scrolling
            _this.scheduleAfterScrollTimer();
        };
        _this.setMessageListRef = function (element) {
            _this.messageList = element;
        };
        _this.onTouchStart = function () {
            _this.isTouchActive = true;
        };
        _this.onTouchEnd = function () {
            _this.isTouchActive = false;
            _this.scheduleAfterScrollTimer();
        };
        return _this;
    }
    MessageList.prototype.componentWillUpdate = function (nextProps, nextState) {
        // shouldComponent update may delay the actual update
    };
    MessageList.prototype.shouldComponentUpdate = function (nextProps, nextState, nextContext) {
        this.loadedMessagesInUpdate = this.loadedMessagesInUpdate || this.props.isLoadingMessages && !nextProps.isLoadingMessages;
        this.captureScrollState();
        if (!this.props.isAppActive && nextProps.isAppActive)
            this.consumeLastMessageIfApplicable();
        // if touch is active, then schedule update for later, after touch/scroll interaction is finished
        if (this.isTouchActive || this.isScrollActive) {
            this.needsForceRenderAfterScroll = true;
            return false;
        }
        return true;
    };
    MessageList.prototype.componentDidUpdate = function () {
        this.invalidateScrollPosition();
    };
    MessageList.prototype.invalidateScrollPosition = function () {
        if (this.keepScrolledToBottom) {
            this.scrollToBottom();
            this.consumeLastMessageIfApplicable();
        }
        else if (this.keepTopScrollPosition) {
            var contentHeightDiff = this.messageList.scrollHeight - this.lastScrollHeight;
            this.messageList.scrollTop = this.lastScrollTop + contentHeightDiff;
        }
    };
    MessageList.prototype.scrollToBottom = function () {
        var _a = this.messageList, scrollHeight = _a.scrollHeight, clientHeight = _a.clientHeight;
        var maxScrollTop = scrollHeight - clientHeight;
        this.messageList.scrollTop = maxScrollTop > 0 ? maxScrollTop : 0;
        this.captureBottomScrollState();
    };
    MessageList.prototype.consumeLastMessageIfApplicable = function () {
        if (this.keepScrolledToBottom && this.props.isAppActive) {
            if (this.props.messages.length > 0) {
                this.props.onConsumeMessageAtIndex(this.props.messages[this.props.messages.length - 1].source.index);
            }
        }
    };
    MessageList.prototype.captureBottomScrollState = function () {
        var _a = this.messageList, scrollTop = _a.scrollTop, scrollHeight = _a.scrollHeight, clientHeight = _a.clientHeight;
        var bottomScrollThreshold = Math.floor(scrollHeight - clientHeight);
        // remember the current scroll state so we can potentially scroll back to the state after messages are loaded
        this.lastScrollHeight = scrollHeight;
        this.lastScrollTop = scrollTop;
        this.keepScrolledToBottom = Math.ceil(scrollTop) >= bottomScrollThreshold;
    };
    MessageList.prototype.captureScrollState = function () {
        this.captureBottomScrollState();
        this.keepTopScrollPosition = !this.keepScrolledToBottom && this.loadedMessagesInUpdate;
    };
    MessageList.prototype.loadMoreMessages = function () {
        // loading already, hold on
        if (this.props.isLoadingMessages)
            return;
        this.props.onLoadMoreMessages();
    };
    MessageList.prototype.scheduleAfterScrollTimer = function () {
        this.isScrollActive = true;
        // clear the timer if we're already running
        clearTimeout(this.afterScrollTimer);
        if (!this.isTouchActive) {
            this.afterScrollTimer = setTimeout(this.onAfterScrollTimer, 100);
        }
    };
    MessageList.prototype.findMyLastMessageIndex = function () {
        for (var index = this.props.messages.length - 1; index >= 0; index--) {
            if (this.props.messages[index].isFromMe)
                return this.props.messages[index].source.index;
        }
        return -1;
    };
    MessageList.prototype.render = function () {
        var _this = this;
        this.needsForceRenderAfterScroll = false;
        this.loadedMessagesInUpdate = false;
        var myLastMessageIndex = 0;
        if (AppConfig.current().channel.message.showReadStatus) {
            myLastMessageIndex = this.findMyLastMessageIndex();
        }
        var messages = this.props.messages.map(function (message, index) {
            return (React.createElement(ChannelItem_1.default, { key: message.source.index, author: _this.props.members.get(message.source.author), message: message, showReadStatus: AppConfig.current().channel.message.showReadStatus && (message.source.index <= _this.props.lastConsumedMessageIndex && message.source.index == myLastMessageIndex) }));
        });
        // insert date separators
        this.injectDateSeparatorElements(messages);
        return (React.createElement("div", { className: "Container " + AppConfig.current().channel.visual.messageStyle, ref: this.setMessageListRef, onScroll: this.onScroll, onTouchEnd: this.onTouchEnd, onTouchStart: this.onTouchStart },
            messages,
            this.renderTypingIndicator()));
    };
    MessageList.prototype.renderTypingIndicator = function () {
        var typingString = "";
        if (this.props.typers.length > 0 && AppConfig.current().channel.showTypingIndicator) {
            typingString = Utils.formatString(Globalization_1.strings().TypingIndicator, Utils.getNameForMember(this.props.typers[0].source.identity, this.props.typers[0]));
        }
        return (React.createElement("div", { className: "TypingIndicator", key: "TypingIndicator" },
            React.createElement("span", null, typingString)));
    };
    MessageList.prototype.isScrolledIntoView = function (el) {
        var elemTop = el.getBoundingClientRect().top;
        var elemBottom = el.getBoundingClientRect().bottom;
        var isVisible = (elemTop >= 0) && (elemBottom <= window.innerHeight);
        return isVisible;
    };
    MessageList.prototype.injectDateSeparatorElements = function (messageElements) {
        var formatSeparatorDate = function (date) {
            if (utils_1.isToday(date)) {
                return Globalization_1.strings().Today;
            }
            else if (utils_1.isYesterday(date)) {
                return Globalization_1.strings().Yesterday;
            }
            else {
                return date.toLocaleDateString();
            }
        };
        var dateSeparator = function (message) {
            var className = "InlineMessage";
            if (!message.groupWithPrevious) {
                className += " InlineMessageNotGrouped";
            }
            return (React.createElement("div", { className: className, key: message.source.timestamp.getTime() },
                React.createElement("div", { className: "DateSeparator" },
                    React.createElement("hr", { className: "SeparatorLine" }),
                    React.createElement("div", { className: "SeparatorText" }, formatSeparatorDate(message.source.timestamp)),
                    React.createElement("hr", { className: "SeparatorLine" }))));
        };
        var lastMessageDate;
        var injectedItems = 0;
        this.props.messages.forEach(function (message, index) {
            if (index == 0 || !utils_1.isSameDate(lastMessageDate, message.source.timestamp)) {
                messageElements.splice(index + injectedItems, 0, dateSeparator(message));
                injectedItems++;
            }
            lastMessageDate = message.source.timestamp;
        });
    };
    return MessageList;
}(React.Component));
MessageList.displayName = "MessageList";
MessageList = __decorate([
    pacomo_1.pacomoDecorator
], MessageList);
exports.default = MessageList;
