/// <reference types="react" />
import * as React from "react";
import "./Chrome.less";
export interface Props {
    readonly channelSid: string;
}
export default class Chrome extends React.PureComponent<Props, undefined> {
    static readonly displayName: string;
    private _styleConfig;
    private closeClick;
    render(): JSX.Element;
}
