declare const _default: {
    InputPlaceHolder: string;
    TypingIndicator: string;
    Connecting: string;
    Disconnected: string;
    Read: string;
    MessageSendingDisabled: string;
    Today: string;
    Yesterday: string;
};
export default _default;
