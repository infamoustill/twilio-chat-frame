# Twilio Chat Frame

Chat Frame
=============

Chat Frame is a library to help you bootstrap Twilio Chat SDK and render a Channel using only a few lines of code.

Instantiating and using Chat Frame
=============

To use the Chat Frame you need [to have a Twilio Programmable Chat Client instance](https://media.twiliocdn.com/sdk/js/chat/v1.2/docs/) and a Channel instance from it.
You can use the Twilio Programmable Chat SDK to create and join channels.

Chat Frame instantiation and loading looks as follows, just make sure you have a div with id "chatContainer" where the Chat Frame will be loaded:
```
function loadFrame(client, channel) {
    // Configuration for Chat Frame
    const frameConfiguration = {
        channel: {
            chrome: {
                closeCallback: channelSid => {
                    chatFrame.unloadChannelBySid(channelSid);
                }
            },
            visual: {
                colorTheme: 'DarkTheme'
            }
        }
    };

    // Create the Chat Frame instance
    const chatFrame = Twilio.Frame.createChat(client, frameConfiguration);
    chatFrame.loadChannel(containerSelector, channel);
}

// Create a Chat Client
Twilio.Chat.Client.create(token).then(client => {
    // Add channel joined listener
    client.on('channelJoined', channel => {
        loadFrame(client, channel);
    });

    // Create a new channel
    return client.createChannel();
})
// Join the created channel
.then(channel => {
    return channel.join();
});
```

Consuming Chat Frame
=============
You can consume the library from NPM:
```
npm install twilio-frame-chat
```

For browser you can use the version from CDN. There are two packages:
1. Frame bundle which includes the JS code and CSS style sheet.
```html
<script src="//media.twiliocdn.com/sdk/js/frame-chat/v0.3/twilio-frame-chat.bundle.min.js"></script>
```
2. Frame JS and CSS style sheet as separate files. You need to include CSS style sheet separately (or create your own).
```html
<link rel="stylesheet" href="//media.twiliocdn.com/sdk/js/frame-chat/v0.3/twilio-frame-chat.css" type="text/css"></link>
<script src="//media.twiliocdn.com/sdk/js/frame-chat/v0.3/twilio-frame-chat.min.js"></script>
```
