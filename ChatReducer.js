"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var redux_1 = require("redux");
var AppConfigReducer = require("./state/ChatConfig");
var ChannelsReducer = require("./state/ChannelsState");
var SessionReducer = require("./state/SessionState");
var chatReducer = redux_1.combineReducers({
    channels: ChannelsReducer.reduce,
    config: AppConfigReducer.reduce,
    session: SessionReducer.reduce
});
exports.chatReducer = chatReducer;
