import { ChannelsState } from "./ChannelsState";
import { ChatConfig } from "./ChatConfig";
import { SessionState } from "./SessionState";
export interface ChatState {
    readonly channels: ChannelsState;
    readonly config: ChatConfig;
    readonly session: SessionState;
}
