"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Globalization_1 = require("../Globalization");
var ChatModule_1 = require("../ChatModule");
/**
 * Color configuration
 * @typedef {Object} ChatFrame#ColorConfig
 * @property {String} [background] CSS to be set as object's background property.
 * @property {String} [color] CSS to be set as object's color (foreground) property
 */
var _jsDocColorConfig = {};
/**
 * Input area color configuration
 * @typedef {Object} ChatFrame#InputAreaColorConfig
 * @extends ChatFrame#ColorConfig
 * @property {ChatFrame#ColorConfig} [placeholder] placeholder color configuration
 */
var _jsDocInputAreaColorConfig = {};
/**
 * Message color configuration
 * @typedef {Object} ChatFrame#MessageColorConfig
 * @property {ChatFrame#ColorConfig} [avatar] avatar color configuration
 * @property {ChatFrame#ColorConfig} [body] color configuration for the message body. Applies to message author and time unless overridden by 'info' property.
 * @property {ChatFrame#ColorConfig} [info] color configuration for the message author and time. If set, then color configuration for message author and time gets overridden.
 */
var _jsDocMessageColorConfig = {};
/**
 * Visual style configuration
 * @typedef {Object} ChatFrame#ChannelStyleConfig
 * @property {ChatFrame#ColorConfig} [header] style configuration for header
 * @property {ChatFrame#InputAreaColorConfig} [inputArea] style configuration for input field
 * @property {ChatFrame#ColorConfig} [main] style configuration for main area (chat)
 * @property {ChatFrame#ColorConfig} [sendButton] style configuration for send message button
 * @property {ChatFrame#MessageColorConfig} [incomingMessage] style configuration for incoming messages
 * @property {ChatFrame#MessageColorConfig} [outgoingMessage] style configuration for outgoing messages
 * @property {ChatFrame#ColorConfig} [readStatus] style configuration for read status indicator
 */
var _jsDocChannelStyleConfig = {};
/**
 * Close callback from a channel
 * @callback ChatFrame#CloseCallback
 * @param {String} sid Channel SID
 */
/**
 * Chat Frame window controls configuration options
 * @typedef {Object} ChatFrame#WindowControls
 * @property {Boolean} [visible] Should the chome bar should be visible
 * @property {ChatFrame#CloseCallback} [closeCallback] Callback function to be called when "Close" button in a channel is clicked.
 */
var _jsDocWindowControls = {};
/**
 * Header configuration options
 * @typedef {Object} ChatFrame#HeaderConfig
 * @property {Boolean} [visible] Should the header be visible
 * @property {Boolean} [image.visible] Should the image in the header be visible
 * @property {String} [image.url] URL of the image to show
 * @property {Boolean} [title.visible] Should the title in the header be visible
 * @property {String} [title.default] Default title to be shown if channel does not have friendly name
 */
var _jsDocHeaderConfig = {};
/**
 * Visual look configuration
 * @typedef {Object} ChatFrame#ChannelVisualConfig
 * @property {('LightTheme'|'MediumTheme'|'DarkTheme')} [colorTheme] Color theme
 * @property {('Rounded'|'Squared'|'Minimal')} [messageStyle] Chat style
 * @property {('Line'|'Bubble'|'Boxed')} [inputAreaStyle] Input area style
 * @property {ChatFrame#ChannelStyleConfig} [override] Visual color overrides
 */
var _jsDocVisualConfig = {};
/**
 * Callback to override message rendering. This callback is invoked every time when
 * attributes or properties of the message change.
 *
 * @callback ChatFrame#RenderMessageCallback
 * @param {Twilio.Chat.Message} message Twilio Chat Message instance. See {@link https://media.twiliocdn.com/sdk/js/chat/v1.0/docs/ Twilio Chat SDK}.
 * @param {Element} element DOM element representing the message. Modify as needed.
 */
/**
 * Message behavior configuration
 * @typedef {Object} ChatFrame#MessageConfig
 * @property {String} [yourDefaultName] Default string shown for local user's name
 * @property {String} [theirDefaultName] Default string shown for remote user's name
 * @property {Boolean} [yourFriendlyNameOverride] Should the user's friendly name be dispalyed as name for this user's messages?
 * @property {Boolean} [theirFriendlyNameOverride] Should the user's friendly name be dispalyed as name for this other user's messages?
 * @property {Boolean} [showReadStatus] Should read status of local user's messages be displayed in the UI?
 * @property {ChatFrame#RenderMessageCallback} [renderCallback] Callback function which is invoked when message is displayed or needs to be updated
 */
var _jsDocMessageConfig = {};
/**
 * Channel Frame configuration
 * @typedef {Object} ChatFrame#ChannelConfig
 * @property {ChatFrame#WindowControls} [windowControls] Window controls configuration options
 * @property {ChatFrame#HeaderConfig} [header] Header configuration options
 * @property {ChatFrame#MessageConfig} [message] Message configuration options
 * @property {ChatFrame#ChannelVisualConfig} [visual] Visual configuration options
 * @property {Boolean} [returnKeySendsMessage] Should the return key be enabled to send a message?
 * @property {Boolean} [showTypingIndicator] Should typing indicator be shown when other members are typing?
 */
var _jsDocChannelConfig = {};
/**
 * Callback to return URL to the avatar picture for message sender/user.
 * @callback ChatFrame#AvatarCallback
 * @return {String} URL to the avatar picture
 * @param {String} identity Identity of a user
 */
/**
 * Chat Frame configuration options
 * @typedef {Object} ChatFrame#ChatConfig
 * @property {String} [language] language code
 * @property {ChatFrame#AvatarCallback} [avatarCallback] Callback to provide avatar URL for message author
 * @property {ChatFrame#ChannelConfig} [channel] Channel configuration
 */
var _jsDocAppConfig = {};
var ACTION_LOAD_CONFIG = "LOAD_CONFIG";
function reduce(state, action) {
    if (state === void 0) { state = {}; }
    switch (action.type) {
        case ACTION_LOAD_CONFIG:
            return action.payload;
        default:
            return state;
    }
}
exports.reduce = reduce;
function current() {
    return ChatModule_1.ChatModule.state.config;
}
exports.current = current;
function tryGet(propCallBack) {
    try {
        return propCallBack();
    }
    catch (e) {
        return undefined;
    }
}
exports.tryGet = tryGet;
var Actions = (function () {
    function Actions() {
    }
    Actions.initConfig = function (config) {
        Globalization_1.Globalization.getInstance().setLanguage(config.language);
        ChatModule_1.ChatModule.dispatch({
            type: ACTION_LOAD_CONFIG,
            payload: config
        });
    };
    return Actions;
}());
exports.Actions = Actions;
