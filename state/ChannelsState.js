"use strict";
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
Object.defineProperty(exports, "__esModule", { value: true });
var ChannelState = require("./ChannelState");
var initialState = {};
function reduce(state, action) {
    if (state === void 0) { state = initialState; }
    if (!action.meta)
        return state;
    var sid = action.meta.channelSid;
    if (!sid)
        return state;
    var oldChannel = state[sid];
    var newChannel = ChannelState.reduce(oldChannel, action);
    // if channel object changed, then update the list as well
    if (oldChannel !== newChannel) {
        var newState = __assign({}, state);
        switch (action.type) {
            // if channel unload action is called then channel needs to be removed from lists
            case ChannelState.ACTION_UNLOAD_CHANNEL: {
                delete newState[sid];
                break;
            }
            default:
                newState[sid] = newChannel;
        }
        return newState;
    }
    return state;
}
exports.reduce = reduce;
