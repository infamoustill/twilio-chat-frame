import { Action, Dispatch } from "redux";
import * as Twilio from "twilio-chat";
export interface SessionState {
    connectionState: string;
    client: Twilio.Client;
    listener: ClientListener;
    isActive: boolean;
}
export interface SessionStateAction extends Action {
    readonly payload?: any;
}
export declare const ACTION_INIT_SESSION = "SESSION_INIT";
export declare const ACTION_CLIENT_CONNECTION = "SESSION_CLIENT_CONNECTION";
export declare const ACTION_APP_ACTIVE = "SESSION_APP_ACTIVE";
export declare function reduce(state: SessionState, action: SessionStateAction): SessionState;
export declare class Actions {
    private static _dispatcher;
    static dispatcher: Dispatch<any>;
    static init(client: Twilio.Client): void;
    static dispatchConnectionState(connectionState: string): void;
    static setActive(isActive: boolean): void;
}
export declare class ClientListener {
    private _client;
    private _listening;
    constructor(client: Twilio.Client);
    start(): void;
    stop(): void;
    isListening(): boolean;
    private handleConnectionStateChanged;
}
export declare function current(): SessionState;
