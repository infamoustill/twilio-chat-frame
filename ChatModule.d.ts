import { Dispatch } from "redux";
import { ChatState } from "./state/ChatState";
export declare class ChatModule {
    private static _dispatcher;
    private static _stateCb;
    static init(stateCb: () => ChatState, dispatcher: Dispatch<any>): void;
    static readonly state: ChatState;
    static dispatch(action: any): any;
}
