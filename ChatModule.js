"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ChatModule = (function () {
    function ChatModule() {
    }
    ChatModule.init = function (stateCb, dispatcher) {
        ChatModule._stateCb = stateCb;
        ChatModule._dispatcher = dispatcher;
    };
    Object.defineProperty(ChatModule, "state", {
        get: function () {
            if (!ChatModule._stateCb)
                throw new Error("ChatModule not initialized");
            return ChatModule._stateCb();
        },
        enumerable: true,
        configurable: true
    });
    ChatModule.dispatch = function (action) {
        if (!ChatModule._dispatcher)
            throw new Error("ChatModule not initialized");
        return ChatModule._dispatcher(action);
    };
    return ChatModule;
}());
exports.ChatModule = ChatModule;
