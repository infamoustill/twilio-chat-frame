"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var redux_1 = require("redux");
var redux_logger_1 = require("redux-logger");
var redux_promise_middleware_1 = require("redux-promise-middleware");
var Config_1 = require("./Config");
var ChatReducer_1 = require("./ChatReducer");
var middleware = Config_1.default.EnableReduxLogger ?
    redux_1.applyMiddleware(redux_promise_middleware_1.default(), redux_logger_1.createLogger()) :
    redux_1.applyMiddleware(redux_promise_middleware_1.default());
exports.store = redux_1.createStore(ChatReducer_1.chatReducer, middleware);
