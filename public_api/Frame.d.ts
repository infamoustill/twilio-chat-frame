import { Client } from "twilio-chat";
import { ChatFrame } from "./ChatFrame";
import { ChatConfig } from "../state/ChatConfig";
/**
 * Create Twilio Frame for Chat
 *
 * @param {Twilio.Chat.Client} chatClient Instance of the Twilio Chat Client. See {@link https://media.twiliocdn.com/sdk/js/chat/v1.0/docs/ Twilio Chat SDK}.
 * @param {ChatFrame#ChatConfig} [userConfiguration] Configuration object
 */
export declare function createChat(client: Client, userConfiguration?: ChatConfig): ChatFrame;
